﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LivingGraph.GUI
{
    public partial class GraphBase : UserControl
    {

        private List<NodeBase> m_Nodes;
        private List<RelationBase> m_Relations;

        public GraphBase()
        {
            InitializeComponent();

            m_Nodes = new List<NodeBase>();
            m_Relations = new List<RelationBase>();

            GUI.NodeBase source = new global::LivingGraph.GUI.NodeBase();

            source.backgroundColor = System.Drawing.Color.White;
            source.Location = new System.Drawing.Point(250, 170);
            source.Radius = 20;

            GUI.NodeBase sink = new global::LivingGraph.GUI.NodeBase();


            sink.backgroundColor = System.Drawing.Color.White;
            sink.Location = new System.Drawing.Point(100, 30);
            sink.Radius = 40;

            GUI.RelationBase relation = new global::LivingGraph.GUI.RelationBase(source, sink);

            m_Nodes.Add(sink);
            m_Nodes.Add(source);
            m_Relations.Add(relation);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            foreach (NodeBase node in m_Nodes)
            {
                node.Paint(e.Graphics);
            }

            foreach (RelationBase relation in m_Relations)
            {
                relation.Paint(e.Graphics);
            }

        }
    }
}
