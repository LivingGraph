﻿using System.Windows.Forms;
using System.Drawing;


namespace LivingGraph.GUI
{

    public class NodeBase
    {

        private Point m_Location;
        private int m_Radius;

        public Point Location
        {
            get { return m_Location; }
            set { m_Location = value; }
        }

        public int Radius
        {
            get { return m_Radius; }
            set { m_Radius = value; }
        }

        public Color backgroundColor = Color.Blue;
        public void Paint(Graphics graphics)
        {
            int penWidth = 4;
            Pen pen = new Pen(Color.Black, 4);

            int fontHeight = 10;
            Font font = new Font("Arial", fontHeight);

            SolidBrush brush = new SolidBrush(backgroundColor);
            graphics.FillEllipse(brush, Location.X - m_Radius, Location.Y - m_Radius, m_Radius*2, m_Radius*2);
            SolidBrush textBrush = new SolidBrush(Color.Black);

            graphics.DrawEllipse(pen, Location.X - m_Radius + (int)penWidth / 2,
              Location.Y - m_Radius + (int)penWidth / 2, m_Radius*2 - penWidth, m_Radius*2 - penWidth);

            //graphics.DrawString(Text, font, textBrush, penWidth,
            //  Height / 2 - fontHeight);

        }
    }
}