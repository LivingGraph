﻿using System.Windows.Forms;
using System.Drawing;


namespace LivingGraph.GUI
{

    public class RelationBase
    {
        NodeBase m_Source;
        NodeBase m_Sink;

    

        public RelationBase(NodeBase source, NodeBase sink)
        {
            m_Source = source;
            m_Sink = sink;
        }

        public Color backgroundColor = Color.Blue;
        public void Paint(Graphics graphics)
        {
            //int penWidth = 4;
            Pen pen = new Pen(Color.Black, 4);

            int fontHeight = 10;
            Font font = new Font("Arial", fontHeight);

            SolidBrush brush = new SolidBrush(backgroundColor);
            //graphics.FillEllipse(brush, 0, 0, Width, Height);
            SolidBrush textBrush = new SolidBrush(Color.Black);

            graphics.DrawLine(pen, m_Source.Location, m_Sink.Location);


        }
    }
}