﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LivingGraphBase
{
    /// <summary>
    /// Graph of nodes and relations
    /// </summary>
    public class Graph
    {
        Node m_RootNode;
        List<Node> m_Nodes;
        List<Relation> m_Relations;

        /// <summary>
        /// default constructor
        /// </summary>
        public Graph()
        {
            m_Nodes = new List<Node>();
            m_RootNode = new Node();
            m_Nodes.Add(m_RootNode);

            m_Relations = new List<Relation>();
        }

        /// <summary>
        /// Get the root node
        /// </summary>
        public Node RootNode
        {
            get { return m_RootNode; }
        }
        public void Relate(Node source, Node target)
        {
            m_Nodes.Add(source);
            m_Nodes.Add(target);

            m_Relations.Add(new Relation(source, target));
        }
    }
}
