﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LivingGraphBase
{
    public class Relation
    {
        private Node m_Source;
        private Node m_Target;

        /// <summary>
        /// default constructor
        /// </summary>
        public Relation()
        {
            m_Source = new Node();
            m_Target = new Node();
        }

        public Relation(Node source, Node target)
        {
            if (source == null || target == null)
            {
                throw new ArgumentException("source or target node is null");
            }
            m_Source = source;
            m_Target = target;
        }

        /// <summary>
        /// Get or set the source node
        /// </summary>
        public Node Source
        {
            get { return m_Source;  }
            set { m_Source = value; }
        }
        /// <summary>
        /// Get or set the target node
        /// </summary>
        public Node Target
        {
            get { return m_Target; }
            set { m_Target = value; }
        }
    }
}
